# spc\_tag

`spc_tag` is a module for reading and writing SNES SPC sound files. This module
supports reading and writing SPC v0.31 files, in both binary and text formats.

This module is a re-implementation of my C library,
[`libspc_tag`](https://gitlab.com/gemarcano/libspc_tag).

## What are SPC files

SPC sound files consist of metadata and some emulation state. Specifically, SPC
files attempt to capture the state of the audio processing unit of the SNES,
which consists of a Sony SPC700 processor with a digital signal processor which
share 64KB of RAM. In reality, the file format does not capture all the
necessary state to play back all possible audio (such as streaming audio that
some games use), but it works for many of them. Practically, what this means is
that an SPC file requires some emulator in order to play back music.

This module does not emulate the SNES hardware, but it allows parsing SPC files
and extracting the data in an organized manner, so that it can be passed onto
an emulator or any kind of display.

## Example Usage

```
TODO
```

## Application Usage

The module includes an application which parses SPC files and prints their
metadata.

## License

This module is released under the following licenses:
 - GPL v2 or later
 - LGPL v2.1 or later

Refer to the `LICENSE` file for more information.
