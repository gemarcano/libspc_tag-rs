// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

use chrono::ParseError;

use std::fmt;
use std::io;
use std::num::ParseIntError;
use std::num::TryFromIntError;
use std::str::Utf8Error;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    Utf8(Utf8Error),
    Parse,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::IO(ref e) => e.fmt(f),
            Self::Utf8(ref e) => e.fmt(f),
            Self::Parse => write!(f, "Invalid value encountered while parsing SPC metadata"),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::IO(err)
    }
}

impl From<Utf8Error> for Error {
    fn from(err: Utf8Error) -> Self {
        Self::Utf8(err)
    }
}

// FIXME custom error type with more detail?
impl From<ParseError> for Error {
    fn from(_: ParseError) -> Self {
        Self::Parse
    }
}

// FIXME custom error type with more detail?
impl From<ParseIntError> for Error {
    fn from(_: ParseIntError) -> Self {
        Self::Parse
    }
}

// FIXME custom error type with more detail?
impl From<TryFromIntError> for Error {
    fn from(_: TryFromIntError) -> Self {
        Self::Parse
    }
}
