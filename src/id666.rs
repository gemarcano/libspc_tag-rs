// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

use crate::error::Error;

use byteorder::LittleEndian;
use byteorder::ReadBytesExt;
use chrono::Datelike;
use num_traits::AsPrimitive;
use num_traits::PrimInt;

use std::fmt;
use std::fmt::Display;
use std::fmt::Formatter;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::str;

/// Describes the way the data in the SPC file is encoded.
///
/// Besides the very beginning of the SPC header, SPC files can have data encoded either as binary
/// data, or as text.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Format {
    Binary,
    Text,
}

/// Describes the emulator used to dump the SPC file.
///
/// Known emulators (<https://wiki.superfamicom.org/id666-format>):
/// * 0 - Unknown
/// * 1 - ZSNES
/// * 2 - Snes9x
/// * 3 - ZST2SPC
/// * 4 - Other
/// * 5 - SNEShout
/// * 6 - ZSNES/W
/// * 7 - Snes9xpp
/// * 8 - SNESGT
#[derive(Debug, Clone, Copy)]
pub enum Emulator {
    Unknown(u8),
    ZSNES,
    Snes9x,
    ZST2SPC,
    Other,
    SNEShout,
    ZSNESW,
    Snes9xpp,
    SNESGT,
}

impl<T: PrimInt + AsPrimitive<u8>> From<T> for Emulator {
    fn from(value: T) -> Self {
        let value: u8 = value.as_();
        match value {
            1 => Self::ZSNES,
            2 => Self::Snes9x,
            3 => Self::ZST2SPC,
            4 => Self::Other,
            5 => Self::SNEShout,
            6 => Self::ZSNESW,
            7 => Self::Snes9xpp,
            8 => Self::SNESGT,
            i => Self::Unknown(i),
        }
    }
}

impl From<Emulator> for u8 {
    fn from(value: Emulator) -> Self {
        match value {
            Emulator::ZSNES => 1,
            Emulator::Snes9x => 2,
            Emulator::ZST2SPC => 3,
            Emulator::Other => 4,
            Emulator::SNEShout => 5,
            Emulator::ZSNESW => 6,
            Emulator::Snes9xpp => 7,
            Emulator::SNESGT => 8,
            Emulator::Unknown(i) => i,
        }
    }
}

impl Display for Emulator {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::ZSNESW => write!(f, "ZSNES/W"),
            _ => write!(f, "{self:?}"),
        }
    }
}

impl Display for Format {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

/// The contents of an ID666 tag.
///
/// The struct members are abstracted behind accessors because some of the members have
/// restrictions that must hold. The setters in particular enforce that only valid values can be
/// set for such variables.
#[derive(Debug)]
pub struct ID666 {
    /// The minor version of the SPC file.
    ///
    /// This value must be at most 2 decimal digits long, so it is limited to values <= 99.
    minor_version: u8,

    /// Whether ID666 metadata is in binary or text format.
    format: Format,

    /// Title of the song.
    ///
    /// This field is meant to hold a maximum of 32 byte characters. This may or may not be null
    /// terminated-- the specification is unclear about this.
    song_title: String,

    /// Title of the game.
    ///
    /// This field is meant to hold a maximum of 32 byte characters. This may or may not be null
    /// terminated-- the specification is unclear about this.
    game_title: String,

    /// Name of the person that dumped this SPC.
    ///
    /// This field is meant to hold a maximum of 16 byte characters. This may or may not be null
    /// terminated-- the specification is unclear about this.
    name_of_dumper: String,

    /// Comments.
    ///
    /// This field is meant to hold a maximum of 32 byte characters. This may or may not be null
    /// terminated-- the specification is unclear about this.
    comments: String,

    /// Date when the SPC was dumped.
    dump_date: chrono::NaiveDate,

    /// Seconds until fade.
    ///
    /// Consists of 3 digits in text mode (999 max), or 3 bytes in binary mode (16777215 max).
    seconds_until_fade: u32,

    /// Length of fade effect in milliseconds.
    ///
    /// Consists of 5 digits in text mode (maximum of 99999 ms), or 4 bytes in binary mode (maximum
    /// of 4294967295 ms).
    fade_length_ms: u32,

    /// Song artist.
    ///
    /// This field is meant to hold a maximum of 32 byte characters. This may or may not be null
    /// terminated-- the specification is unclear about this.
    song_artist: String,

    /// Default channel disables
    ///
    /// Honestly, I don't know what this is supposed to be, but it's in the specification. It is
    /// true if the default channels should be disabled, I think.
    default_channel_disables: bool,

    /// Numeric representation of which emulator was used to dump this SPC.
    ///
    /// Known emulators (<https://wiki.superfamicom.org/id666-format>):
    /// * 0 - Unknown
    /// * 1 - ZSNES
    /// * 2 - Snes9x
    /// * 3 - ZST2SPC
    /// * 4 - Other
    /// * 5 - SNEShout
    /// * 6 - ZSNES/W
    /// * 7 - Snes9xpp
    /// * 8 - SNESGT
    emulator_used_to_dump: Emulator,
}

/// Sets the target string equal to a second string, only if the second string is less than the
/// length provided.
fn set_string_with_length(target: &mut String, string: String, len: usize) -> Result<(), Error> {
    if string.len() > len {
        return Err(Error::Parse);
    }
    *target = string;
    Ok(())
}

impl Default for ID666 {
    fn default() -> Self {
        Self::new()
    }
}

impl ID666 {
    /// Gets the minor version of the ID666 tag.
    #[must_use]
    pub const fn get_minor_version(&self) -> u8 {
        self.minor_version
    }

    /// Sets the minor version of the ID666 tag to the value specified.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the `minor_version` parameter is greater than 99.
    pub fn set_minor_version(&mut self, minor_version: u8) -> Result<(), Error> {
        if minor_version > 99 {
            return Err(Error::Parse);
        }
        self.minor_version = minor_version;
        Ok(())
    }

    /// Gets the format of the ID666 tag.
    #[must_use]
    pub const fn get_format(&self) -> Format {
        self.format
    }

    /// Sets the format of the ID666 tag.
    ///
    /// This is only useful when writing out the ID666 tag to disk, as in this library both formats
    /// share the same structure in memory.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if some field cannot be used by the new format. Specifically, the
    /// [`Format::Text`] mode has limits on the following fields:
    ///  - `dump_date` : Must be less than or equal to 9999
    ///  - `seconds_until_fade` : Must be less than or equal to 999
    ///  - `fade_length_ms` : Must be less than or equal to 99999
    ///
    /// [`Format::Binary`] has no such restrictions.
    pub fn set_format(&mut self, format: Format) -> Result<(), Error> {
        if self.format == format {
            return Ok(());
        }

        // Should always be safe to convert from Text to Binary, but not the case the other way
        // around
        if matches!(format, Format::Text) {
            // This is a little absurd, but technically possible, what if year is more than 4
            // digits?
            if self.dump_date.year() > 9999 {
                return Err(Error::Parse);
            }

            if self.seconds_until_fade > 999 {
                return Err(Error::Parse);
            }

            if self.fade_length_ms > 99999 {
                return Err(Error::Parse);
            }
        }
        self.format = format;
        Ok(())
    }

    /// Gets the song title recorded in the ID666 tag.
    #[must_use]
    pub fn get_song_title(&self) -> &str {
        &self.song_title
    }

    /// Sets the song title recorded in the ID666 tag.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the given title is greater than 32 bytes in length.
    pub fn set_song_title(&mut self, song_title: String) -> Result<(), Error> {
        set_string_with_length(&mut self.song_title, song_title, 32)
    }

    /// Gets the game title recorded in the ID666 tag.
    #[must_use]
    pub fn get_game_title(&self) -> &str {
        &self.game_title
    }

    /// Sets the game title in the ID666 tag.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the given name is greater than 32 bytes in length.
    pub fn set_game_title(&mut self, game_title: String) -> Result<(), Error> {
        set_string_with_length(&mut self.game_title, game_title, 32)
    }

    /// Returns the name or handle of the individual that dumped the SPC data.
    #[must_use]
    pub fn get_name_of_dumper(&self) -> &str {
        &self.name_of_dumper
    }

    /// Sets the dumper name field of the ID666 to the parameter provided.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the given name is greater than 16 bytes in length.
    pub fn set_name_of_dumper(&mut self, name_of_dumper: String) -> Result<(), Error> {
        set_string_with_length(&mut self.name_of_dumper, name_of_dumper, 16)
    }

    /// Gets the comments in the ID666 tag.
    #[must_use]
    pub fn get_comments(&self) -> &str {
        &self.comments
    }

    /// Sets the comments field in the ID666 tag.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the length of the parameter provide is more than 32 bytes.
    pub fn set_comments(&mut self, comments: String) -> Result<(), Error> {
        set_string_with_length(&mut self.comments, comments, 32)
    }

    /// Gets the date field indicating when the ID666 tag was created.
    #[must_use]
    pub const fn get_dump_date(&self) -> &chrono::NaiveDate {
        &self.dump_date
    }

    /// Sets the date field of the ID666 tag.
    ///
    /// # Errors
    ///
    /// [`Error::Parse`] if the archive format is [`Format::Text`] and the year is greater than
    /// 9999, as that is the largest year [`Format::Text`] supports. [`Format::Binary`] can go up
    /// to 65535.
    pub fn set_dump_date(&mut self, date: chrono::NaiveDate) -> Result<(), Error> {
        if matches!(self.format, Format::Text) && date.year() > 9999 {
            return Err(Error::Parse);
        }
        self.dump_date = date;
        Ok(())
    }

    /// Gets the ID666 field indicating the number of seconds before fading.
    #[must_use]
    pub const fn get_seconds_until_fade(&self) -> u32 {
        self.seconds_until_fade
    }

    /// Sets the ID666 field indicating the number of seconds before fading.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the input value is greater than 999 and the ID666 is configured
    /// in [`Format::Text`] mode, due to the limitation imposed by the text format of a maximum
    /// length before fade of 999 seconds. In [`Format::Binary`] mode, [`Error::Parse`] is returned
    /// if the value is above what can be represented in an unsigned 24 bit number, or larger than
    /// 16777215
    pub fn set_seconds_until_fade(&mut self, seconds_until_fade: u32) -> Result<(), Error> {
        // Limited by Text format 3 digit size for this field
        if matches!(self.format, Format::Text) && seconds_until_fade > 999 {
            return Err(Error::Parse);
        }

        // Binary is 3 bytes in size, or (2^24 - 1) max size
        if seconds_until_fade > 16_777_215 {
            return Err(Error::Parse);
        }

        self.seconds_until_fade = seconds_until_fade;
        Ok(())
    }

    /// Gets the length of the fade field in the ID666 tag, in milliseconds.
    #[must_use]
    pub const fn get_fade_length_ms(&self) -> u32 {
        self.fade_length_ms
    }

    /// Sets the ID666 field for the fade length in milliseconds.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the input value is greater than 99999 and the ID666 is
    /// configured in [`Format::Text`] mode, due to the limitation imposed by the text format of a
    /// maximum maximum fade length of 99999 milliseconds.
    pub fn set_fade_length_ms(&mut self, fade_length_ms: u32) -> Result<(), Error> {
        // Limited by Text format 5 digit size for this field
        if matches!(self.format, Format::Text) && fade_length_ms > 99999 {
            return Err(Error::Parse);
        }
        self.fade_length_ms = fade_length_ms;
        Ok(())
    }

    /// Gets the ID666 song artist field.
    #[must_use]
    pub fn get_song_artist(&self) -> &str {
        &self.song_artist
    }

    /// Sets the ID666 song artist field.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the input string is longer than 32 bytes long.
    pub fn set_song_artist(&mut self, song_artist: String) -> Result<(), Error> {
        set_string_with_length(&mut self.song_artist, song_artist, 32)
    }

    /// Gets the ID666 default channel disabled field.
    #[must_use]
    pub const fn get_default_channel_disables(&self) -> bool {
        self.default_channel_disables
    }

    /// Sets the ID666 default channel disabled field.
    pub fn set_default_channel_disables(&mut self, disables: bool) {
        self.default_channel_disables = disables;
    }

    /// Gets the ID666 emulator field.
    #[must_use]
    pub const fn get_emulator_used_to_dump(&self) -> Emulator {
        self.emulator_used_to_dump
    }

    /// Sets the ID666 emulator field.
    pub fn set_emulator_used_to_dump(&mut self, emulator: Emulator) {
        self.emulator_used_to_dump = emulator;
    }

    /// Returns a new ID666 tag.
    ///
    /// All fields are initialized as empty or 0 where possible, the dump date is set to Jan 1,
    /// 1900, the minor version is set to 31, and the emulator field is set to unknown.
    #[must_use]
    pub fn new() -> Self {
        Self {
            minor_version: 31,
            format: Format::Binary,
            song_title: String::new(),
            game_title: String::new(),
            name_of_dumper: String::new(),
            comments: String::new(),
            dump_date: chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap(),
            seconds_until_fade: 0,
            fade_length_ms: 0,
            song_artist: String::new(),
            default_channel_disables: false,
            emulator_used_to_dump: Emulator::Unknown(0),
        }
    }

    /// Creates an ID666 tag with the arguments provided.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if any of the parameters cannot be used. See the documentation for
    /// the many `set_` functions for possible errors.
    #[allow(clippy::too_many_arguments)]
    pub fn with_fields(
        minor_version: u8,
        format: Format,
        song_title: String,
        game_title: String,
        name_of_dumper: String,
        comments: String,
        dump_date: chrono::NaiveDate,
        seconds_until_fade: u32,
        fade_length_ms: u32,
        song_artist: String,
        default_channel_disables: bool,
        emulator_used_to_dump: Emulator,
    ) -> Result<Self, Error> {
        let mut result = Self::new();
        result.set_minor_version(minor_version)?;
        result.set_format(format)?;
        result.set_song_title(song_title)?;
        result.set_game_title(game_title)?;
        result.set_name_of_dumper(name_of_dumper)?;
        result.set_comments(comments)?;
        result.set_dump_date(dump_date)?;
        result.set_seconds_until_fade(seconds_until_fade)?;
        result.set_fade_length_ms(fade_length_ms)?;
        result.set_song_artist(song_artist)?;
        result.set_default_channel_disables(default_channel_disables);
        result.set_emulator_used_to_dump(emulator_used_to_dump);
        Ok(result)
    }
}

pub trait ID666Write {
    /// Writes the id666 metadata to self.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the ID666 data being read is corrupted or malformatted, or if
    /// the fields being read are beyond what the field supports (See the `set_` methods for
    /// the conditions that can cause field parsing to fail).
    ///
    /// Returns [`Error::IO`] if some kind of IO error occured while reading or seeking the object
    /// from which the ID666 data is being extracted.
    fn write_id666(&mut self, id666: &ID666) -> Result<(), Error>;
}

fn write_string(io: &mut impl Write, string: &str, len: usize) -> Result<(), Error> {
    if string.len() > len {
        return Err(Error::Parse);
    }
    io.write_all(string.as_bytes())?;
    io.write_all(vec![0; len - string.len()].as_slice())?;
    Ok(())
}

impl<T: Write + Seek> ID666Write for T {
    fn write_id666(&mut self, id666: &ID666) -> Result<(), Error> {
        self.seek(SeekFrom::Start(0x23))?;
        self.write_all(&[26, 31])?;

        self.seek(SeekFrom::Start(0x2E))?;
        write_string(self, &id666.song_title, 32)?;
        write_string(self, &id666.game_title, 32)?;
        write_string(self, &id666.name_of_dumper, 16)?;
        write_string(self, &id666.comments, 32)?;

        let date = id666.dump_date;

        // Binary and Text formats have different limits. Other functions should have made sure the
        // values in the ID666 tag are valid for the current mode
        match id666.format {
            Format::Binary => {
                let year = date.year() as u32;
                let month = date.month();
                let day = date.day();
                let result = year << 16 | month << 8 | day;
                self.write_all(&result.to_le_bytes())?;

                self.seek(SeekFrom::Start(0xA9))?;
                self.write_all(&id666.seconds_until_fade.to_le_bytes()[0..3])?;
                self.write_all(&id666.fade_length_ms.to_le_bytes())?;
                write_string(self, &id666.song_artist, 32)?;
                let disables: u8 = u8::from(id666.default_channel_disables);
                self.write_all(&[disables])?;
                self.write_all(&[u8::from(id666.emulator_used_to_dump)])?;
                // beginning of reserved section
                self.write_all(&[0])?;
            }
            Format::Text => {
                let year = date.year();
                let month = date.month();
                let day = date.day();
                self.write_all(format!("{month:02}/{day:02}/{year:04}\0").as_bytes())?;
                self.write_all(format!("{:\0<3}", id666.seconds_until_fade).as_bytes())?;
                self.write_all(format!("{:\0<5}", id666.fade_length_ms).as_bytes())?;
                write_string(self, &id666.song_artist, 32)?;
                let disables: u8 = u8::from(id666.default_channel_disables);
                self.write_all(&[disables])?;
                // This will break if there are more than 10 SPC dumpers
                self.write_all(format!("{}", u8::from(id666.emulator_used_to_dump)).as_bytes())?;
            }
        }
        // reserved section
        self.write_all(&[0; 45])?;

        Ok(())
    }
}

fn check_binary(io: &mut (impl Read + Seek)) -> Result<Format, Error> {
    let current_pos = io.stream_position()?;
    io.seek(SeekFrom::Start(0xD2))?;
    let null = io.read_u8()?; // The value being read here should be null for a binary format
    io.seek(SeekFrom::Start(current_pos))?;
    match null {
        0 => Ok(Format::Binary),
        _ => Ok(Format::Text),
    }
}

fn get_string_number(buf: &[u8]) -> Result<u32, Error> {
    let str_ = str::from_utf8(buf)?;
    let str_ = str_.trim();
    let str_ = str_.trim_matches(char::from(0));
    Ok(str_.parse::<u32>()?)
}

pub trait ID666Read {
    /// Parses an ID666 tag from some object with SPC file data implementing [`Read`] and
    /// [`Seek`]. It is possible that a valid SPC file may not have an ID666 tag, in which case
    /// this function will return [`Option::None`].
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the ID666 data being read is corrupted or malformatted, or if
    /// the fields being read are beyond what the field supports (See the `set_` methods for
    /// the conditions that can cause field parsing to fail).
    ///
    /// Returns [`Error::Utf8`] if one of the ID666 string fields cannot be parsed as a UTF8
    /// string.
    ///
    /// Returns [`Error::IO`] if some kind of IO error occured while reading or seeking the object
    /// from which the ID666 data is being extracted.
    fn read_id666(&mut self) -> Result<Option<ID666>, Error>;
}

impl<T: Read + Seek> ID666Read for T {
    fn read_id666(&mut self) -> Result<Option<ID666>, Error> {
        // Rest of the header begins at 0x23
        self.seek(SeekFrom::Start(0x23))?;
        let hasid666 = self.read_u8()?;
        let minor = self.read_u8()?;
        // Version minor must be two decimal characters or smaller
        if minor > 99 {
            return Err(Error::Parse);
        }

        let id666: Option<ID666> = if hasid666 == 0x1a {
            // Skip to beginning of actual ID666 tag
            self.seek(SeekFrom::Start(0x2E))?;
            // ... and now begin reading and parsing data
            let mut buf: [u8; 32] = [0; 32];
            self.read_exact(&mut buf)?;
            let song_title = str::from_utf8(&buf)?.to_string();
            self.read_exact(&mut buf)?;
            let game_title = str::from_utf8(&buf)?.to_string();
            self.read_exact(&mut buf[..16])?;
            let name_of_dumper = str::from_utf8(&buf[..16])?.to_string();
            self.read_exact(&mut buf)?;
            let comments = str::from_utf8(&buf)?.to_string();

            let seconds_until_fade: u32;
            let fade_length_ms: u32;
            let song_artist: String;
            let default_channel_disables: bool;
            let emulator_used_to_dump: Emulator;
            let date: chrono::NaiveDate;

            let kind = check_binary(self)?;
            match kind {
                Format::Binary => {
                    let raw_date = self.read_u32::<LittleEndian>()?;
                    // data is stored as a binary coded decimal
                    let day = raw_date & 0xFF;
                    let month = (raw_date >> 8) & 0xFF;
                    let year = (raw_date >> 16) as i32;
                    date = chrono::NaiveDate::from_ymd_opt(year, month, day)
                        .unwrap_or_else(|| chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap());
                    // Skip over unused space
                    self.seek(SeekFrom::Start(0xA9))?;
                    seconds_until_fade = self.read_u24::<LittleEndian>()?;
                    fade_length_ms = self.read_u32::<LittleEndian>()?;
                    let mut buf = [0u8; 32];
                    self.read_exact(&mut buf)?;
                    song_artist = str::from_utf8(&buf)?.to_string();
                    let default_channel_disables_ = self.read_u8()?;
                    if default_channel_disables_ > 1 {
                        return Err(Error::Parse);
                    }
                    default_channel_disables = default_channel_disables_ == 1;
                    let emulator: i32 = self.read_u8()?.into();
                    emulator_used_to_dump = Emulator::from(emulator);
                }
                Format::Text => {
                    let mut buf = [0u8; 32];
                    self.read_exact(&mut buf[..11])?;
                    let date_ =
                        chrono::NaiveDate::parse_from_str(str::from_utf8(&buf[..10])?, "%m/%d/%Y");
                    date = date_
                        .unwrap_or_else(|_| chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap());

                    self.read_exact(&mut buf[..3])?;
                    seconds_until_fade = get_string_number(&buf[..3])?;
                    self.read_exact(&mut buf[..5])?;
                    fade_length_ms = get_string_number(&buf[..5])?;
                    self.read_exact(&mut buf)?;
                    song_artist = str::from_utf8(&buf)?.to_string();
                    let default_channel_d = self.read_u8()?;
                    // Man, the SPC spec sucks. Some SPC files I've encountered use binary numbers
                    // here (or maybe it's just null?). Allow also binary numbers...
                    if default_channel_d != b'0'
                        && default_channel_d != b'1'
                        && default_channel_d > 1
                    {
                        return Err(Error::Parse);
                    }
                    default_channel_disables = default_channel_d == b'1';
                    self.read_exact(&mut buf[..1])?;
                    // Even if we encounter an unknown emulator, don't error out-- the spec might
                    // have evolved even while this implementation hasn't
                    let emulator_used_to_dump_ = get_string_number(&buf[..1])?;
                    emulator_used_to_dump = Emulator::from(emulator_used_to_dump_ as i32);
                }
            }

            Some(ID666::with_fields(
                minor,
                kind,
                song_title,
                game_title,
                name_of_dumper,
                comments,
                date,
                seconds_until_fade,
                fade_length_ms,
                song_artist,
                default_channel_disables,
                emulator_used_to_dump,
            )?)
        } else {
            None
        };

        Ok(id666)
    }
}

#[cfg(test)]
mod tests {
    use crate::error::Error;
    use crate::id666::Emulator;
    use crate::id666::Format;
    use crate::id666::ID666Read;
    use crate::id666::ID666Write;
    use crate::id666::ID666;

    use std::env;
    use std::fs::File;
    use std::io::Cursor;
    use std::io::Read;
    use std::io::Seek;
    use std::io::SeekFrom;
    use std::io::Write;
    use std::path::Path;
    use std::str;

    #[derive(Clone)]
    struct ID666Text {
        song_title: [u8; 32],
        game_title: [u8; 32],
        dumper_name: [u8; 16],
        comments: [u8; 32],
        date_dumped: [u8; 11],
        seconds_until_fade: [u8; 3],
        fade_length_ms: [u8; 5],
        artist: [u8; 32],
        default_channel_disables: u8,
        dump_emulator: u8,
    }

    #[derive(Clone)]
    struct ID666Binary {
        song_title: [u8; 32],
        game_title: [u8; 32],
        dumper_name: [u8; 16],
        comments: [u8; 32],
        date_dumped: [u8; 4],
        seconds_until_fade: [u8; 3],
        fade_length_ms: [u8; 4],
        artist: [u8; 32],
        default_channel_disables: u8,
        dump_emulator: u8,
    }

    trait ID666Data {
        fn reset(&mut self);
        fn write_text(&mut self, data: ID666Text);
        fn write_binary(&mut self, data: ID666Binary);
    }

    impl Default for ID666Text {
        fn default() -> Self {
            ID666Text {
                song_title: [0; 32],
                game_title: [0; 32],
                dumper_name: [0; 16],
                comments: [0; 32],
                date_dumped: [0; 11],
                seconds_until_fade: [0; 3],
                fade_length_ms: [0; 5],
                artist: [0; 32],
                default_channel_disables: 0,
                dump_emulator: 0,
            }
        }
    }

    impl Default for ID666Binary {
        fn default() -> Self {
            ID666Binary {
                song_title: [0; 32],
                game_title: [0; 32],
                dumper_name: [0; 16],
                comments: [0; 32],
                date_dumped: [0; 4],
                seconds_until_fade: [0; 3],
                fade_length_ms: [0; 4],
                artist: [0; 32],
                default_channel_disables: 0,
                dump_emulator: 0,
            }
        }
    }

    impl<T: Read + Write + Seek> ID666Data for T {
        fn reset(&mut self) {
            self.seek(SeekFrom::Start(0)).unwrap();
            self.write("SNES-SPC700 Sound File Data v0.31".as_bytes())
                .unwrap();
            self.write(&[26, 26, 27, 31]).unwrap();
        }

        fn write_text(&mut self, data: ID666Text) {
            self.seek(SeekFrom::Start(0x23)).unwrap();
            self.write(&[26]).unwrap();
            self.seek(SeekFrom::Start(0x2E)).unwrap();
            self.write(&data.song_title).unwrap();
            self.write(&data.game_title).unwrap();
            self.write(&data.dumper_name).unwrap();
            self.write(&data.comments).unwrap();
            self.write(&data.date_dumped).unwrap();
            self.write(&data.seconds_until_fade).unwrap();
            self.write(&data.fade_length_ms).unwrap();
            self.write(&data.artist).unwrap();
            self.write(&data.default_channel_disables.to_string().as_bytes())
                .unwrap();
            self.write(&data.dump_emulator.to_string().as_bytes())
                .unwrap();
            self.seek(SeekFrom::Start(0x100 - 1)).unwrap();
            self.write(&[0]).unwrap();
        }

        fn write_binary(&mut self, data: ID666Binary) {
            self.seek(SeekFrom::Start(0x23)).unwrap();
            self.write(&[26]).unwrap();
            self.seek(SeekFrom::Start(0x2E)).unwrap();
            self.write(&data.song_title).unwrap();
            self.write(&data.game_title).unwrap();
            self.write(&data.dumper_name).unwrap();
            self.write(&data.comments).unwrap();
            self.write(&data.date_dumped).unwrap();
            self.seek(SeekFrom::Start(0xA9)).unwrap();
            self.write(&data.seconds_until_fade).unwrap();
            self.write(&data.fade_length_ms).unwrap();
            self.write(&data.artist).unwrap();
            self.write(&data.default_channel_disables.to_le_bytes())
                .unwrap();
            self.write(&data.dump_emulator.to_le_bytes()).unwrap();
            self.seek(SeekFrom::Start(0x100 - 1)).unwrap();
            self.write(&[0]).unwrap();
        }
    }

    fn setup(filename: &str) -> Cursor<Vec<u8>> {
        let root = env::var("CARGO_MANIFEST_DIR").unwrap();
        let test_dir = Path::new(&root).join("resources/test");
        let mut file = File::open(test_dir.join(filename)).unwrap();
        let mut data = Vec::new();
        file.read_to_end(&mut data).unwrap();
        Cursor::new(data)
    }

    fn test_parse(data: &mut (impl Read + Seek)) -> Option<ID666> {
        let id666 = data.read_id666();
        assert!(id666.is_ok());
        let id666 = id666.unwrap();
        id666
    }

    #[test]
    fn test_binary() {
        let mut spc = setup("binary.spc");
        let id666 = test_parse(&mut spc);
        assert!(matches!(id666, Some(_)));
    }

    #[test]
    fn test_text() {
        let mut spc = setup("text.spc");
        let id666 = test_parse(&mut spc);
        assert!(matches!(id666, Some(_)));
    }

    #[test]
    fn test_bad1() {
        // This should be a file with the same length as binary.spc, but filled with garbage, so a
        // completely invalid SPC file
        let mut spc = setup("bad1.spc");
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Parse)));
    }

    #[test]
    fn test_bad2() {
        // This should be a size 0 file
        let mut spc = setup("bad2.spc");
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::IO(_))));
    }

    #[test]
    fn test_empty() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let id666 = spc.read_id666().unwrap();
        assert!(matches!(id666, None));
    }

    #[test]
    fn test_text2() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let mut text: ID666Text = Default::default();
        text.song_title[..5].copy_from_slice("title".as_bytes());
        text.game_title[..4].copy_from_slice("game".as_bytes());
        text.dumper_name[..6].copy_from_slice("dumper".as_bytes());
        text.comments[..10].copy_from_slice("no comment".as_bytes());
        text.date_dumped[..10].copy_from_slice("11/22/3333".as_bytes());
        text.seconds_until_fade[..2].copy_from_slice("44".as_bytes());
        text.fade_length_ms[..4].copy_from_slice("1000".as_bytes());
        text.artist[..6].copy_from_slice("artist".as_bytes());
        text.default_channel_disables = 0;
        text.dump_emulator = 1;
        spc.write_text(text);
        let id666 = spc.read_id666().unwrap();
        assert!(matches!(id666, Some(_)));
    }

    #[test]
    fn test_binary2() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let mut binary: ID666Binary = Default::default();
        binary.song_title[..5].copy_from_slice("title".as_bytes());
        binary.game_title[..4].copy_from_slice("game".as_bytes());
        binary.dumper_name[..6].copy_from_slice("dumper".as_bytes());
        binary.comments[..10].copy_from_slice("no comment".as_bytes());
        binary.date_dumped = 0x33331122u32.to_le_bytes();
        binary
            .seconds_until_fade
            .copy_from_slice(&44u32.to_le_bytes()[..3]);
        binary.fade_length_ms = 1000u32.to_le_bytes();
        binary.artist[..6].copy_from_slice("artist".as_bytes());
        binary.default_channel_disables = 0;
        binary.dump_emulator = 1;
        spc.write_binary(binary);
        let id666 = spc.read_id666().unwrap();
        assert!(matches!(id666, Some(_)));
    }

    #[test]
    fn test_bad_text() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let mut text: ID666Text = Default::default();
        text.song_title[..5].copy_from_slice("title".as_bytes());
        text.game_title[..4].copy_from_slice("game".as_bytes());
        text.dumper_name[..6].copy_from_slice("dumper".as_bytes());
        text.comments[..10].copy_from_slice("no comment".as_bytes());
        text.date_dumped[..10].copy_from_slice("11/22/3333".as_bytes());
        text.seconds_until_fade[..2].copy_from_slice("44".as_bytes());
        text.fade_length_ms[..4].copy_from_slice("1000".as_bytes());
        text.artist[..6].copy_from_slice("artist".as_bytes());
        text.default_channel_disables = 0;
        text.dump_emulator = 1;

        let clean = text.clone();

        text.song_title[31] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.game_title[31] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.dumper_name[15] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.comments[31] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.date_dumped[2] = "\\".as_bytes()[0];
        spc.write_text(text);
        let id666 = spc.read_id666().unwrap().unwrap();
        assert_eq!(
            id666.dump_date,
            chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap()
        );

        let mut text = clean.clone();
        text.date_dumped[1] = "5".as_bytes()[0];
        spc.write_text(text);
        let id666 = spc.read_id666().unwrap().unwrap();
        assert_eq!(
            id666.dump_date,
            chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap()
        );

        let mut text = clean.clone();
        text.date_dumped[2] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.seconds_until_fade[1] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.fade_length_ms[1] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.artist[31] = 0xFF;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut text = clean.clone();
        text.default_channel_disables = 2;
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Parse)));

        let mut text = clean.clone();
        text.dump_emulator = 9;
        spc.write_text(text);
        let id666 = spc.read_id666().unwrap().unwrap();
        assert!(matches!(
            id666.get_emulator_used_to_dump(),
            Emulator::Unknown(9)
        ));

        let text = clean.clone();
        spc.write_text(text);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Ok(_)));
    }

    #[test]
    fn test_bad_binary() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let mut binary: ID666Binary = Default::default();
        binary.song_title[..5].copy_from_slice("title".as_bytes());
        binary.game_title[..4].copy_from_slice("game".as_bytes());
        binary.dumper_name[..6].copy_from_slice("dumper".as_bytes());
        binary.comments[..10].copy_from_slice("no comment".as_bytes());
        binary.date_dumped = 0x33331122u32.to_le_bytes();
        binary
            .seconds_until_fade
            .copy_from_slice(&44u32.to_le_bytes()[..3]);
        binary.fade_length_ms = 1000u32.to_le_bytes();
        binary.artist[..6].copy_from_slice("artist".as_bytes());
        binary.default_channel_disables = 0;
        binary.dump_emulator = 1;

        let clean = binary.clone();

        binary.song_title[31] = 0xFF;
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut binary = clean.clone();
        binary.game_title[31] = 0xFF;
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut binary = clean.clone();
        binary.dumper_name[15] = 0xFF;
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut binary = clean.clone();
        binary.comments[31] = 0xFF;
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut binary = clean.clone();
        binary.date_dumped[1] = 99;
        spc.write_binary(binary);
        let id666 = spc.read_id666().unwrap().unwrap();
        assert_eq!(
            id666.dump_date,
            chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap()
        );

        // seconds_until_fade and fade_length_ms are valid for their entire range, so no need to
        // test them

        let mut binary = clean.clone();
        binary.artist[31] = 0xFF;
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Utf8(_))));

        let mut binary = clean.clone();
        binary.default_channel_disables = 2;
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Err(Error::Parse)));

        let mut binary = clean.clone();
        binary.dump_emulator = 9;
        spc.write_binary(binary);
        let id666 = spc.read_id666().unwrap().unwrap();
        assert!(matches!(
            id666.get_emulator_used_to_dump(),
            Emulator::Unknown(9)
        ));

        let binary = clean.clone();
        spc.write_binary(binary);
        let id666 = spc.read_id666();
        assert!(matches!(id666, Ok(_)));
    }

    #[test]
    fn test_write_id666() {
        // Setup some data manually...
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let mut binary: ID666Binary = Default::default();
        binary.song_title[..5].copy_from_slice("title".as_bytes());
        binary.game_title[..4].copy_from_slice("game".as_bytes());
        binary.dumper_name[..6].copy_from_slice("dumper".as_bytes());
        binary.comments[..10].copy_from_slice("no comment".as_bytes());
        binary.date_dumped = 0x0D050B16u32.to_le_bytes();
        binary
            .seconds_until_fade
            .copy_from_slice(&44u32.to_le_bytes()[..3]);
        binary.fade_length_ms = 1000u32.to_le_bytes();
        binary.artist[..6].copy_from_slice("artist".as_bytes());
        binary.default_channel_disables = 0;
        binary.dump_emulator = 1;
        spc.write_binary(binary);

        // Parse the data, to make sure we wrote it correctly
        let mut spc2 = Cursor::new(Vec::new());
        let mut id666 = spc.read_id666().unwrap().unwrap();
        assert!(spc2.write_id666(&id666).is_ok());

        let id6662 = spc2.read_id666();
        assert!(id6662.is_ok());
        let id6662 = id6662.unwrap();
        assert!(id6662.is_some());
        let id6662 = id6662.unwrap();
        assert_eq!("title", id6662.song_title.trim_end_matches(['\0']));
        assert_eq!("game", id6662.game_title.trim_end_matches(['\0']));
        assert_eq!("dumper", id6662.name_of_dumper.trim_end_matches(['\0']));
        assert_eq!("no comment", id6662.comments.trim_end_matches(['\0']));
        assert_eq!(
            chrono::NaiveDate::from_ymd_opt(3333, 11, 22).unwrap(),
            id6662.dump_date
        );
        assert_eq!(44, id6662.seconds_until_fade);
        assert_eq!(1000, id6662.fade_length_ms);
        assert_eq!("artist", id6662.song_artist.trim_end_matches(['\0']));
        assert_eq!(false, id6662.default_channel_disables);
        assert!(matches!(id6662.emulator_used_to_dump, Emulator::ZSNES));

        // Now, test that switching formats works
        assert_eq!(id666.get_format(), Format::Binary);
        assert!(id666.set_format(Format::Text).is_ok());
        assert_eq!(id666.get_format(), Format::Text);
        // And write the new format
        assert!(spc2.write_id666(&id666).is_ok());

        // And now start extracting from the raw data to confirm we wrote in text mode correctly
        spc2.seek(SeekFrom::Start(0x9E)).unwrap();
        let mut date = [0u8; 11];
        spc2.read_exact(&mut date).unwrap();

        let raw = spc2.get_ref();
        assert_eq!("title\0", str::from_utf8(&raw[0x2E..0x2E + 6]).unwrap());
        assert_eq!("game\0", str::from_utf8(&raw[0x4E..0x4E + 5]).unwrap());
        assert_eq!("dumper\0", str::from_utf8(&raw[0x6E..0x6E + 7]).unwrap());
        assert_eq!(
            "no comment\0",
            str::from_utf8(&raw[0x7E..0x7E + 11]).unwrap()
        );
        assert_eq!("11/22/3333\0", str::from_utf8(&date).unwrap());
        assert_eq!("44\0", str::from_utf8(&raw[0xA9..0xA9 + 3]).unwrap());
        assert_eq!("1000\0", str::from_utf8(&raw[0xAC..0xAC + 5]).unwrap());
        assert_eq!("artist\0", str::from_utf8(&raw[0xB1..0xB1 + 7]).unwrap());
        assert_eq!(0u8, raw[0xD1]);
        assert_eq!("1", str::from_utf8(&[raw[0xD2]]).unwrap());
    }

    #[test]
    fn test_format_id666() {
        let mut spc = setup("binary.spc");
        let mut id666 = test_parse(&mut spc).unwrap();
        assert!(id666.set_format(Format::Text).is_ok());
        assert!(id666
            .set_dump_date(chrono::NaiveDate::from_ymd_opt(9999, 12, 31).unwrap())
            .is_ok());
        assert!(id666
            .set_dump_date(chrono::NaiveDate::from_ymd_opt(10000, 12, 31).unwrap())
            .is_err());
        assert!(id666.set_seconds_until_fade(999).is_ok());
        assert!(id666.set_seconds_until_fade(1000).is_err());
        assert!(id666.set_fade_length_ms(99999).is_ok());
        assert!(id666.set_fade_length_ms(100000).is_err());
        assert!(id666.set_format(Format::Binary).is_ok());
        assert!(id666
            .set_dump_date(chrono::NaiveDate::from_ymd_opt(9999, 12, 31).unwrap())
            .is_ok());
        assert!(id666
            .set_dump_date(chrono::NaiveDate::from_ymd_opt(10000, 12, 31).unwrap())
            .is_ok());
        assert!(id666.set_seconds_until_fade(999).is_ok());
        assert!(id666.set_seconds_until_fade(1000).is_ok());
        assert!(id666.set_fade_length_ms(99999).is_ok());
        assert!(id666.set_fade_length_ms(100000).is_ok());
        assert!(id666.set_format(Format::Binary).is_ok());
        assert!(id666.set_format(Format::Text).is_err());
        assert!(id666.set_seconds_until_fade(999).is_ok());
        assert!(id666.set_format(Format::Text).is_err());
        assert!(id666.set_fade_length_ms(99999).is_ok());
        assert!(id666.set_format(Format::Text).is_err());
        assert!(id666
            .set_dump_date(chrono::NaiveDate::from_ymd_opt(9999, 12, 31).unwrap())
            .is_ok());
        assert!(id666.set_format(Format::Text).is_ok());
    }
}
