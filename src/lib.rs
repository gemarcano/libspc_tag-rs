// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

//! This crate contains routines to read and write Super Nintendo SPC audio file/dumps, including
//! XID666 tags.
//!
//! This has a companion cli application crate that will read an SPC file and print out its
//! information.
//!
//! This is based off my original library in C, found at
//! <https://gitlab.com/gemarcano/libspc_tag.git>. Both the C library and this rust implementation
//! should be very similar in function, although rust has definitely force me to handle some corner
//! cases that I did not handle correctly in my C library.

pub mod error;
pub mod id666;
pub mod metadata;
pub mod spc;
pub mod xid666;

pub use crate::id666::ID666;
pub use crate::metadata::Metadata;
pub use crate::metadata::MetadataRead;
pub use crate::metadata::MetadataWrite;
pub use crate::spc::SPCRead;
pub use crate::spc::SPCWrite;
pub use crate::spc::SPC;
pub use crate::xid666::XID666;
