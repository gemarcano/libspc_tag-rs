// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

use spc_tag::id666::Emulator;
use spc_tag::xid666::XID666Subchunk;
use spc_tag::MetadataRead;

use std::env;
use std::fs::File;

pub fn main() {
    let args: Vec<String> = env::args().collect();
    let mut f = File::open(&args[1]).unwrap();
    let spc = match f.read_spc_metadata() {
        Ok(spc) => spc,
        Err(err) => {
            eprintln!("Error: {err}");
            return;
        }
    };
    println!("[");
    println!("\t{{");

    if spc.id666.is_some() {
        let id666 = spc.id666.unwrap();
        println!("\t\t\"minor version\": {},", id666.get_minor_version());
        println!(
            "\t\t\"song title\": \"{}\",",
            id666.get_song_title().trim_matches('\0')
        );
        println!(
            "\t\t\"game title\": \"{}\",",
            id666.get_game_title().trim_matches('\0')
        );
        println!(
            "\t\t\"name of dumper\": \"{}\",",
            id666.get_name_of_dumper().trim_matches('\0')
        );
        println!(
            "\t\t\"comments\": \"{}\",",
            id666.get_comments().trim_matches('\0')
        );
        println!(
            "\t\t\"dump date\": \"{}\",",
            id666.get_dump_date().format("%Y-%m-%d")
        );
        println!(
            "\t\t\"seconds until fade\": {},",
            id666.get_seconds_until_fade()
        );
        println!("\t\t\"fade length (ms)\": {},", id666.get_fade_length_ms());
        println!("\t\t\"song artist\": \"{}\",", id666.get_song_artist());
        println!(
            "\t\t\"default channel disables\": {},",
            i32::from(id666.get_default_channel_disables())
        );
        println!(
            "\t\t\"emulator used to dump\": {},",
            u8::from(id666.get_emulator_used_to_dump())
        );
    }

    if spc.xid666.is_some() {
        let xid666 = spc.xid666.unwrap();
        println!("\t\txid666\": {{");
        for subchunk in &xid666 {
            print!("\t\t\t");
            match subchunk {
                XID666Subchunk::SongName(name) => println!("\"song name\": \"{name}\""),
                XID666Subchunk::GameName(name) => println!("\"game name\": \"{name}\""),
                XID666Subchunk::ArtistName(name) => println!("\"artist name\": \"{name}\""),
                XID666Subchunk::DumperName(name) => println!("\"dumper name\": \"{name}\""),
                XID666Subchunk::DateDumped(name) => println!("\"date dumped\": \"{name}\""),
                XID666Subchunk::Emulator(emulator) => {
                    let name = match emulator {
                        Emulator::ZSNES => "ZSNES".into(),
                        Emulator::Snes9x => "Snes9x".into(),
                        Emulator::ZST2SPC => "ZST2SPC".into(),
                        Emulator::Other => "Other".into(),
                        Emulator::SNEShout => "SNEShout".into(),
                        Emulator::ZSNESW => "ZSNES/W".into(),
                        Emulator::Snes9xpp => "Snes9xpp".into(),
                        Emulator::SNESGT => "SNESGT".into(),
                        Emulator::Unknown(i) => format!("Unknown({i})"),
                    };
                    println!("game name: \"{name}\"");
                }
                XID666Subchunk::Comments(comments) => println!("\"comments\": \"{comments}\""),
                XID666Subchunk::OfficialTitle(title) => println!("\"official title\": \"{title}\""),
                XID666Subchunk::DiscNumber(disc) => println!("\"disc number\": {disc}"),
                XID666Subchunk::TrackNumber(num) => println!("\"track number\": {num}"),
                XID666Subchunk::PublisherName(name) => println!("\"publisher\": \"{name}\""),
                XID666Subchunk::CopyrightYear(year) => println!("\"copyright year\": {year}"),
                XID666Subchunk::IntroLength(len) => println!("\"introduction length\": {len}"),
                XID666Subchunk::LoopLength(len) => println!("\"loop length\": {len}"),
                XID666Subchunk::EndLength(len) => println!("\"end length\": {len}"),
                XID666Subchunk::FadeLength(len) => println!("\"fade length\": {len}"),
                XID666Subchunk::MutedVoices(muted) => println!("\"muted voices\": {muted}"),
                XID666Subchunk::LoopAmount(amount) => println!("\"loop amount\": {amount}"),
                XID666Subchunk::MixingLevel(level) => println!("\"mixing level\": {level}"),
            }
        }
        println!("\t\t}}");
    }
    println!("\t}}");
    println!("]");
}
