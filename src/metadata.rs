// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

use crate::error::Error;
use crate::id666::ID666Read;
use crate::id666::ID666Write;
use crate::id666::ID666;
use crate::xid666::XID666Read;
use crate::xid666::XID666Write;
use crate::xid666::XID666;

use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::str::from_utf8;

/// Represents the metadata held by an SPC file, optinally holding an ID666 and XID666 headers.
#[derive(Debug)]
pub struct Metadata {
    /// An optional ID666 tag (an SPC file is not required to have this).
    pub id666: Option<ID666>,
    /// An optional XID666 tag (an SPC file is not required to have this).
    pub xid666: Option<XID666>,
}

fn check_beginning(io: &mut (impl Read + Seek)) -> Result<(), Error> {
    let current_pos = io.stream_position()?;
    io.seek(SeekFrom::Start(0))?;
    let mut header: [u8; 33] = [0; 33];
    io.read_exact(&mut header)?;

    let header_str = from_utf8(&header);
    // Just in this case, if there was an error parsing the magic string, report it as a parse
    // error instead of a Utf8 error
    if header_str.is_err() {
        return Err(Error::Parse);
    }
    let header_str = header_str.unwrap();
    let mut fixed: [u8; 2] = [0; 2];
    io.read_exact(&mut fixed)?;
    io.seek(SeekFrom::Start(current_pos))?;

    if header_str[0..29].eq("SNES-SPC700 Sound File Data v") && fixed == [0x1a; 2] {
        Ok(())
    } else {
        Err(Error::Parse)
    }
}

pub trait MetadataRead {
    /// Parses the SPC file metadata from the object provided, returning a Metadata object with the
    /// SPC file metadata.
    ///
    /// # Errors
    ///
    /// Returns the same errors that [`ID666Read`] and [`XID666Read`] return.
    fn read_spc_metadata(&mut self) -> Result<Metadata, Error>;
}

impl<T: Read + Seek> MetadataRead for T {
    fn read_spc_metadata(&mut self) -> Result<Metadata, Error> {
        // Check file header, and 2 magic bytes
        check_beginning(self)?;
        let id666 = self.read_id666()?;
        let xid666 = self.read_xid666()?;
        Ok(Metadata { id666, xid666 })
    }
}

pub trait MetadataWrite {
    /// Writes the metadata to self.
    ///
    /// # Errors
    ///
    /// Returns the same errors that [`ID666Write`] and [`XID666Write`] return.
    fn write_spc_metadata(&mut self, metadata: &Metadata) -> Result<(), Error>;
}

impl<T: ID666Write + XID666Write + Write + Seek> MetadataWrite for T {
    fn write_spc_metadata(&mut self, metadata: &Metadata) -> Result<(), Error> {
        self.seek(SeekFrom::Start(0))?;
        self.write_all("SNES-SPC700 Sound File Data v0.31".as_bytes())?;
        self.write_all(&[26u8, 26u8])?;
        if let Some(id666) = &metadata.id666 {
            self.write_id666(id666)?;
        }
        if let Some(xid666) = &metadata.xid666 {
            self.write_xid666(xid666)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::error::Error;
    use crate::metadata::Metadata;
    use crate::metadata::MetadataRead;
    use crate::metadata::MetadataWrite;

    use std::env;
    use std::fs::File;
    use std::io::Cursor;
    use std::io::Read;
    use std::io::Seek;
    use std::io::SeekFrom;
    use std::io::Write;
    use std::path::Path;

    fn setup(filename: &str) -> Cursor<Vec<u8>> {
        let root = env::var("CARGO_MANIFEST_DIR").unwrap();
        let test_dir = Path::new(&root).join("resources/test");
        let mut file = File::open(test_dir.join(filename)).unwrap();
        let mut data = Vec::new();
        file.read_to_end(&mut data).unwrap();
        Cursor::new(data)
    }

    fn test_parse(data: &mut (impl Read + Seek)) -> Metadata {
        let metadata = data.read_spc_metadata();
        assert!(metadata.is_ok());
        metadata.unwrap()
    }

    #[test]
    fn test_binary() {
        let mut spc = setup("binary.spc");
        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, Some(_)));
        assert!(matches!(metadata.xid666, Some(_)));
    }

    #[test]
    fn test_text() {
        let mut spc = setup("text.spc");
        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, Some(_)));
        assert!(matches!(metadata.xid666, Some(_)));
    }

    #[test]
    fn test_bad1() {
        // This should be a file with the same length as binary.spc, but filled with garbage, so a
        // completely invalid SPC file
        let mut spc = setup("bad1.spc");
        let metadata = spc.read_spc_metadata();
        assert!(matches!(metadata, Err(Error::Parse)));
    }

    #[test]
    fn test_bad2() {
        // This should be a size 0 file
        let mut spc = setup("bad2.spc");
        let metadata = spc.read_spc_metadata();
        assert!(matches!(metadata, Err(Error::IO(_))));
    }

    #[test]
    fn test_text_no_xid() {
        let spc = setup("text.spc");
        let mut vec = spc.into_inner();
        vec.resize(0x10200, 0);
        let mut spc = Cursor::new(vec);
        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, Some(_)));
        assert!(matches!(metadata.xid666, None));

        spc.seek(SeekFrom::Start(0x23)).unwrap();
        spc.write(&[0x27]).unwrap();

        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, None));
        assert!(matches!(metadata.xid666, None));
    }

    #[test]
    fn test_binary_no_xid() {
        let spc = setup("binary.spc");
        let mut vec = spc.into_inner();
        vec.resize(0x10200, 0);
        let mut spc = Cursor::new(vec);
        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, Some(_)));
        assert!(matches!(metadata.xid666, None));
    }

    #[test]
    fn test_binary_neither_id() {
        let spc = setup("binary.spc");
        let mut vec = spc.into_inner();
        vec.resize(0x10200, 0);

        let mut spc = Cursor::new(vec);
        spc.seek(SeekFrom::Start(0x23)).unwrap();
        spc.write(&[0x27]).unwrap();

        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, None));
        assert!(matches!(metadata.xid666, None));
    }

    #[test]
    fn test_text_neither_id() {
        let spc = setup("text.spc");
        let mut vec = spc.into_inner();
        vec.resize(0x10200, 0);

        let mut spc = Cursor::new(vec);
        spc.seek(SeekFrom::Start(0x23)).unwrap();
        spc.write(&[0x27]).unwrap();

        let metadata = test_parse(&mut spc);
        assert!(matches!(metadata.id666, None));
        assert!(matches!(metadata.xid666, None));
    }

    #[test]
    fn test_text_no_id() {
        let mut spc = setup("text.spc");
        spc.seek(SeekFrom::Start(0x23)).unwrap();
        spc.write(&[0x27]).unwrap();
        let metadata = test_parse(&mut spc);

        assert!(matches!(metadata.id666, None));
        assert!(matches!(metadata.xid666, Some(_)));
    }

    #[test]
    fn test_binary_no_id() {
        let mut spc = setup("binary.spc");
        spc.seek(SeekFrom::Start(0x23)).unwrap();
        spc.write(&[0x27]).unwrap();
        let metadata = test_parse(&mut spc);

        assert!(matches!(metadata.id666, None));
        assert!(matches!(metadata.xid666, Some(_)));
    }

    #[test]
    fn test_write() {
        let mut spc = setup("binary.spc");
        let metadata = test_parse(&mut spc);
        let mut output = Cursor::new(Vec::new());
        assert!(output.write_spc_metadata(&metadata).is_ok());
        let raw = output.get_ref();
        assert_eq!("SNES-SPC700 Sound File Data v0.31".as_bytes(), &raw[0..33]);
        assert_eq!(&[26, 26u8], &raw[33..35]);
        let metadata = test_parse(&mut output);
        assert!(metadata.id666.is_some());
        assert!(metadata.xid666.is_some());
    }
}
