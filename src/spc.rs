// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

use crate::error::Error;
use crate::metadata::Metadata;
use crate::metadata::MetadataRead;
use crate::metadata::MetadataWrite;

use byteorder::LittleEndian;
use byteorder::ReadBytesExt;
use byteorder::WriteBytesExt;

use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

pub struct SPC {
    /// Metadata describing the SPC file. Contains optional ID666 and XID666 fields.
    pub metadata: Metadata,
    /// The PC register of the SPC700 when dumped.
    pub pc: u16,
    /// The A register of the SPC700 when dumped.
    pub a: u8,
    /// The X register of the SPC700 when dumped.
    pub x: u8,
    /// The Y register of the SPC700 when dumped.
    pub y: u8,
    /// The PSW register of the SPC700 when dumped.
    pub psw: u8,
    /// The lower 8 bits of the SP register of the SPC700 when dumped.
    pub sp: u8,
    /// The RAM of the SPC700 when dumped.
    pub ram: [u8; 65536],
    /// The DSP registers when dumped.
    pub dsp_regs: [u8; 128],
    /// Extra RAM (IPL ROM area) of the SPC700 when dumped.
    pub extra_ram: [u8; 64],
}

pub trait SPCRead {
    /// Reads the SPC file from self.
    ///
    /// # Errors
    ///
    /// Any errors returned by [`MetadataRead::read_spc_metadata`]
    ///
    /// [`Error::IO`] if there are any underlying IO errors.
    fn read_spc(&mut self) -> Result<SPC, Error>;
}

impl<T: MetadataRead + Read + Seek> SPCRead for T {
    fn read_spc(&mut self) -> Result<SPC, Error> {
        let metadata = self.read_spc_metadata()?;
        self.seek(SeekFrom::Start(0x25))?;
        let pc = self.read_u16::<LittleEndian>()?;
        let a = self.read_u8()?;
        let x = self.read_u8()?;
        let y = self.read_u8()?;
        let psw = self.read_u8()?;
        let sp = self.read_u8()?;
        let mut ram: [u8; 65536] = [0; 65536];
        let mut dsp_regs: [u8; 128] = [0; 128];
        let mut extra_ram: [u8; 64] = [0; 64];
        self.seek(SeekFrom::Start(0x100))?;
        self.read_exact(&mut ram)?;
        self.read_exact(&mut dsp_regs)?;
        self.seek(SeekFrom::Start(0x101C0))?;
        self.read_exact(&mut extra_ram)?;
        Ok(SPC {
            metadata,
            pc,
            a,
            x,
            y,
            psw,
            sp,
            ram,
            dsp_regs,
            extra_ram,
        })
    }
}

pub trait SPCWrite {
    /// Parses the SPC file from the object provided, returning an SPC object with the contents of
    /// the SPC file, including both metadata, registers, and memory.
    ///
    /// # Errors
    ///
    /// Returns the same errors that [`ID666Read::read_id666`](crate::id666::ID666Read) and
    /// [`XID666Read::read_xid666`](crate::id666::ID666Read) return. Additionally, it will return
    /// [`Error::IO`] if the file is too short.
    fn write_spc(&mut self, spc: &SPC) -> Result<(), Error>;
}

impl<T: MetadataWrite + Write + Seek> SPCWrite for T {
    fn write_spc(&mut self, spc: &SPC) -> Result<(), Error> {
        self.write_spc_metadata(&spc.metadata)?;
        self.seek(SeekFrom::Start(0x25))?;
        self.write_u16::<LittleEndian>(spc.pc)?;
        self.write_u8(spc.a)?;
        self.write_u8(spc.x)?;
        self.write_u8(spc.y)?;
        self.write_u8(spc.psw)?;
        self.write_u8(spc.sp)?;
        self.write_u16::<LittleEndian>(0)?;

        self.seek(SeekFrom::Start(0x100))?;
        self.write_all(&spc.ram)?;
        self.write_all(&spc.dsp_regs)?;
        self.write_all(&[0u8; 64])?;
        self.write_all(&spc.extra_ram)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::error::Error;
    use crate::spc::SPCRead;
    use crate::spc::SPCWrite;
    use crate::spc::SPC;

    use std::env;
    use std::fs::File;
    use std::io::Cursor;
    use std::io::Read;
    use std::io::Seek;
    use std::path::Path;

    fn setup(filename: &str) -> Cursor<Vec<u8>> {
        let root = env::var("CARGO_MANIFEST_DIR").unwrap();
        let test_dir = Path::new(&root).join("resources/test");
        let mut file = File::open(test_dir.join(filename)).unwrap();
        let mut data = Vec::new();
        file.read_to_end(&mut data).unwrap();
        Cursor::new(data)
    }

    fn test_parse(data: &mut (impl Read + Seek)) -> SPC {
        let spc = data.read_spc();
        assert!(spc.is_ok());
        spc.unwrap()
    }

    #[test]
    fn test_binary() {
        let mut spc_file = setup("binary.spc");
        let spc = test_parse(&mut spc_file);
        assert!(matches!(spc.metadata.id666, Some(_)));
        assert!(matches!(spc.metadata.xid666, Some(_)));
        assert_eq!(spc.pc, 0x0201);
        assert_eq!(spc.a, 3);
        assert_eq!(spc.x, 4);
        assert_eq!(spc.y, 5);
        assert_eq!(spc.psw, 6);
        assert_eq!(spc.sp, 7);
    }

    #[test]
    fn test_text() {
        let mut spc_file = setup("text.spc");
        let spc = test_parse(&mut spc_file);
        assert!(matches!(spc.metadata.id666, Some(_)));
        assert!(matches!(spc.metadata.xid666, Some(_)));
        assert_eq!(spc.pc, 0x0201);
        assert_eq!(spc.a, 3);
        assert_eq!(spc.x, 4);
        assert_eq!(spc.y, 5);
        assert_eq!(spc.psw, 6);
        assert_eq!(spc.sp, 7);
    }

    #[test]
    fn test_bad1() {
        // This should be a file with the same length as binary.spc, but filled with garbage, so a
        // completely invalid SPC file
        let mut spc_file = setup("bad1.spc");
        let spc = spc_file.read_spc();
        assert!(matches!(spc, Err(Error::Parse)));
    }

    #[test]
    fn test_bad2() {
        // This should be a size 0 file
        let mut spc_file = setup("bad2.spc");
        let spc = spc_file.read_spc();
        assert!(matches!(spc, Err(Error::IO(_))));
    }

    #[test]
    fn test_incomplete_ram() {
        let spc_file = setup("binary.spc");
        let mut vec = spc_file.into_inner();
        vec.resize(0x10000, 0);
        let mut spc_file = Cursor::new(vec);
        let spc = spc_file.read_spc();
        assert!(matches!(spc, Err(Error::IO(_))));
    }

    #[test]
    fn test_write() {
        let mut spc_file = setup("binary.spc");
        let mut spc = test_parse(&mut spc_file);
        spc.ram.fill(0xAA);
        spc.dsp_regs.fill(0xBB);
        spc.extra_ram.fill(0xCC);
        let mut output = Cursor::new(Vec::new());
        assert!(output.write_spc(&spc).is_ok());
        let spc = test_parse(&mut output);

        assert_eq!(spc.pc, 0x0201);
        assert_eq!(spc.a, 3);
        assert_eq!(spc.x, 4);
        assert_eq!(spc.y, 5);
        assert_eq!(spc.psw, 6);
        assert_eq!(spc.sp, 7);
        assert_eq!(spc.ram, [0xAAu8; 0x10000]);
        assert_eq!(spc.dsp_regs, [0xBBu8; 128]);
        assert_eq!(spc.extra_ram, [0xCCu8; 64]);
    }
}
