// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later OR MPL-2.0
// SPDX-FileCopyrightText: 2022-2024 Gabriel Marcano <gabemarcano@yahoo.com>

use crate::error::Error;
use crate::id666::Emulator;

use byteorder::LittleEndian;
use byteorder::ReadBytesExt;
use chrono::Datelike;
use chrono::NaiveDate;

use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::str;

/// Describes XID666 subchunks.
///
/// Each XID666 subchunk is associated with some sort of tag data.
///
/// All strings in subchunks are null-terminated in the SPC file.
#[derive(Debug)]
pub enum XID666Subchunk {
    SongName(String),
    GameName(String),
    ArtistName(String),
    DumperName(String),
    DateDumped(NaiveDate),
    Emulator(Emulator),
    Comments(String),
    OfficialTitle(String),
    DiscNumber(u16),
    TrackNumber(u16),
    PublisherName(String),
    CopyrightYear(i16),
    IntroLength(u32),
    LoopLength(u32),
    EndLength(u32),
    FadeLength(u32),
    MutedVoices(u16),
    LoopAmount(u16),
    MixingLevel(u16),
}

/// Represents an XID666 chunk, which consists of zero or more subchunks. Each subchunk contains a
/// metadata field.
#[derive(Debug)]
pub struct XID666 {
    chunk: Vec<XID666Subchunk>,
}

impl XID666 {
    /// Adds an XID666 subchunk to the XID666 chunk.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the year in a [`XID666Subchunk::DateDumped`] is less than 0, or
    /// if the string data of any subchunk with a string is smaller than 3 bytes or longer than 255
    /// bytes (allowing for 1 byte for the mandatory null character, which is added when being
    /// written to disk).
    pub fn push(&mut self, subchunk: XID666Subchunk) -> Result<(), Error> {
        // FIXME add restrictions. For example, date's year must be positive
        match subchunk {
            // Enforce that the year of the date must be positive
            XID666Subchunk::DateDumped(date) => {
                if date.year() < 0 {
                    return Err(Error::Parse);
                }
            }
            // Enforce that strings must have between 4 and 256 characters, null character included
            XID666Subchunk::SongName(ref string)
            | XID666Subchunk::GameName(ref string)
            | XID666Subchunk::ArtistName(ref string)
            | XID666Subchunk::DumperName(ref string)
            | XID666Subchunk::Comments(ref string)
            | XID666Subchunk::OfficialTitle(ref string)
            | XID666Subchunk::PublisherName(ref string) => {
                if string.len() < 3 || string.len() > 255 {
                    return Err(Error::Parse);
                }
            }
            _ => { /* ignore, no restriction */ }
        }
        self.chunk.push(subchunk);
        Ok(())
    }

    /// Returns the length of the XID666 chunk in bytes.
    #[must_use]
    pub fn len(&self) -> usize {
        self.chunk.len()
    }

    /// Returns whether the XID666 chunk is empty or not.
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.chunk.is_empty()
    }
}

impl<'a> IntoIterator for &'a XID666 {
    type Item = <&'a Vec<XID666Subchunk> as IntoIterator>::Item;
    type IntoIter = <&'a Vec<XID666Subchunk> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.chunk.iter()
    }
}

impl<'a> XID666 {
    #[must_use]
    pub fn iter(&'a self) -> <&'a Vec<XID666Subchunk> as IntoIterator>::IntoIter {
        <&Self as IntoIterator>::into_iter(self)
    }
}

pub trait XID666Read {
    /// Reads and parses data from the given object containing an SPC file, and returns the parsed
    /// XID666 chunk. If there is no XID666 chunk, the function returns [`Option::None`].
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the XID666 chunk or one of its subchunks is malformed or
    /// corrupted.
    ///
    /// Returns [`Error::IO`] if some kind of IO error happened while reading and seeking through
    /// the SPC file.
    ///
    /// Returns [`Error::Utf8`] if there was a problem encoding string data from a subchunk into
    /// UTF8.
    fn read_xid666(&mut self) -> Result<Option<XID666>, Error>;
}

impl<T: Read + Seek> XID666Read for T {
    fn read_xid666(&mut self) -> Result<Option<XID666>, Error> {
        // Check if there's an xid666 -- only way to do this is to seek to the end of the header,
        // and try to read
        let seek_result = self.seek(SeekFrom::Start(0x10200));
        let read_result = self.read_u32::<LittleEndian>();

        if seek_result.is_err() || read_result.is_err() {
            return Ok(None);
        }

        // Jump to end of regular ID666
        self.seek(SeekFrom::Start(0x10200))?;

        let mut buf: [u8; 4] = [0; 4];
        self.read_exact(&mut buf)?;
        // This is the one time we'll catch the UTF8 parse error and do a non-standard conversion
        // if necessary. This is because if the parsing fails here, we don't have a valid XID666
        // chunk, period, so report it as a Parse error
        let magic = str::from_utf8(&buf);
        if magic.is_err() || magic.unwrap() != "xid6" {
            return Err(Error::Parse);
        }

        let chunk_size = self.read_u32::<LittleEndian>()?;
        let mut size_left = chunk_size;
        let mut chunk = Vec::<XID666Subchunk>::new();
        while size_left > 0 {
            // Parse subchunks
            // First byte is ID
            let id = self.read_u8()?;
            let type_ = self.read_u8()?;
            let data = self.read_u16::<LittleEndian>()?;
            size_left -= 4;
            let data = match type_ {
                // Data, so two bytes already read are the actual tag information
                0 => match id {
                    0x6 => XID666Subchunk::Emulator(data.into()),
                    0x11 => XID666Subchunk::DiscNumber(data),
                    0x12 => XID666Subchunk::TrackNumber(data),
                    0x14 => XID666Subchunk::CopyrightYear(data as i16),
                    0x34 => XID666Subchunk::MutedVoices(data),
                    0x35 => XID666Subchunk::LoopAmount(data),
                    0x36 => XID666Subchunk::MixingLevel(data),
                    _ => return Err(Error::Parse),
                },
                // String, so the two bytes already read are the string size, including null
                // terminator. Subchunks are 4 byte aligned, so after the string, if it isn't
                // aligned, there may be additional null characters until the next subchunks is 4
                // byte aligned.
                1 => {
                    let size = data;
                    // XID strings can't be more than 256 characters
                    if !(4..=256).contains(&size) {
                        return Err(Error::Parse);
                    }
                    let mut data = String::new();
                    let read = self.take(size.into()).read_to_string(&mut data)?;
                    // Make sure the amount of data read matches what we expect, else we hit EOF
                    // and that means we have a bad SPC file
                    if read != size.into() {
                        return Err(Error::Parse);
                    }

                    let size: u32 = size.into();
                    // All contents of data are 4-bytes aligned. Ensure the current pointer is 4
                    // bytes aligned if necessary, if the end of the string isn't enough to be
                    // aligned
                    let padding = 4 - size % 4;
                    if padding != 4 {
                        size_left -= padding;
                        self.seek(SeekFrom::Current(padding.into()))?;
                    }
                    size_left -= size;
                    // We don't need the string to be null terminated in Rust, get rid of null
                    // termination-- we just need to remember it when we write the string back to
                    // disk
                    data.truncate(data.trim_end_matches('\0').len());
                    match id {
                        0x1 => XID666Subchunk::SongName(data),
                        0x2 => XID666Subchunk::GameName(data),
                        0x3 => XID666Subchunk::ArtistName(data),
                        0x4 => XID666Subchunk::DumperName(data),
                        0x7 => XID666Subchunk::Comments(data),
                        0x10 => XID666Subchunk::OfficialTitle(data),
                        0x13 => XID666Subchunk::PublisherName(data),
                        _ => return Err(Error::Parse),
                    }
                }
                4 => {
                    let size = data;
                    if size != 4 {
                        return Err(Error::Parse);
                    }
                    let data = self.read_u32::<LittleEndian>()?;
                    size_left -= 4;

                    match id {
                        0x5 => {
                            // data is stored as a binary coded decimal
                            let day = data & 0xFF;
                            let month = (data >> 8) & 0xFF;
                            let year = (data >> 16) as i32;
                            let date = chrono::NaiveDate::from_ymd_opt(year, month, day)
                                .unwrap_or_else(|| {
                                    chrono::NaiveDate::from_ymd_opt(1900, 1, 1).unwrap()
                                });
                            XID666Subchunk::DateDumped(date)
                        }
                        0x30 => XID666Subchunk::IntroLength(data),
                        0x31 => XID666Subchunk::LoopLength(data),
                        0x32 => XID666Subchunk::EndLength(data),
                        0x33 => XID666Subchunk::FadeLength(data),
                        _ => return Err(Error::Parse),
                    }
                }
                _ => {
                    return Err(Error::Parse);
                }
            };
            chunk.push(data);
        }
        Ok(Some(XID666 { chunk }))
    }
}

pub trait XID666Write {
    /// Writes an XID666 chunk into Self. If there's no XID666 data, nothing is written.
    ///
    /// # Errors
    ///
    /// Returns [`Error::Parse`] if the XID666 chunk or one of its subchunks is malformed or
    /// corrupted.
    ///
    /// Returns [`Error::IO`] if some kind of IO error happened while reading and seeking through
    /// the SPC file.
    ///
    /// Returns [`Error::Utf8`] if there was a problem encoding string data from a subchunk into
    /// UTF8.

    fn write_xid666(&mut self, xid666: &XID666) -> Result<(), Error>;
}

fn subchunk_string_bytes(id: u8, string: &str) -> Vec<u8> {
    // The requirement that strings must have between 4 and 256 characters, null byte included, is
    // enforced in push.
    let len: u16 = string.len() as u16;
    let mut subchunk = Vec::new();
    subchunk.append(&mut [[id, 0x1], len.to_le_bytes()].concat());
    subchunk.extend_from_slice(string.as_bytes());
    subchunk.push(0u8);
    if subchunk.len() % 4 != 0 {
        let left = 4 - (subchunk.len() % 4);
        subchunk.append(&mut vec![0u8; left]);
    }
    subchunk
}

fn subchunk_integer_bytes(id: u8, number: i32) -> Vec<u8> {
    let mut subchunk = Vec::new();
    subchunk.extend_from_slice(&[id, 0x4, 0x04, 0x00]);
    subchunk.extend_from_slice(&number.to_le_bytes());
    subchunk
}

fn subchunk_uinteger_bytes(id: u8, number: u32) -> Vec<u8> {
    // Should be safe, only using number for its binary contents
    subchunk_integer_bytes(id, number as i32)
}

fn subchunk_data_bytes(id: u8, data: u16) -> Vec<u8> {
    let mut subchunk = Vec::new();
    subchunk.extend_from_slice(&[id, 0x0]);
    subchunk.extend_from_slice(&data.to_le_bytes());
    subchunk
}

impl<T: Write + Seek> XID666Write for T {
    fn write_xid666(&mut self, xid666: &XID666) -> Result<(), Error> {
        self.seek(SeekFrom::Start(0x10200))?;
        self.write_all(b"xid6")?;
        let mut chunk_size: u32 = 0;
        self.seek(SeekFrom::Start(0x10208))?;

        for subchunk in xid666 {
            let bytes = match subchunk {
                XID666Subchunk::SongName(name) => subchunk_string_bytes(1, name),
                XID666Subchunk::GameName(name) => subchunk_string_bytes(2, name),
                XID666Subchunk::ArtistName(name) => subchunk_string_bytes(3, name),
                XID666Subchunk::DumperName(name) => subchunk_string_bytes(4, name),
                XID666Subchunk::DateDumped(date) => {
                    // year must be nonnegative, enforced by XID666::push
                    let year = date.year() as u32;
                    let month = date.month();
                    let day = date.day();
                    let result = year << 16 | month << 8 | day;
                    subchunk_uinteger_bytes(0x5, result)
                }
                XID666Subchunk::Emulator(emulator) => {
                    subchunk_data_bytes(0x6, u8::from(*emulator).into())
                }
                XID666Subchunk::Comments(comment) => subchunk_string_bytes(7, comment),
                XID666Subchunk::OfficialTitle(title) => subchunk_string_bytes(0x10, title),
                XID666Subchunk::DiscNumber(number) => subchunk_data_bytes(0x11, *number),
                XID666Subchunk::TrackNumber(number) => subchunk_data_bytes(0x12, *number),
                XID666Subchunk::PublisherName(name) => subchunk_string_bytes(0x13, name),
                XID666Subchunk::CopyrightYear(year) => subchunk_data_bytes(0x14, *year as u16),
                XID666Subchunk::IntroLength(len) => subchunk_uinteger_bytes(0x30, *len),
                XID666Subchunk::LoopLength(len) => subchunk_uinteger_bytes(0x31, *len),
                XID666Subchunk::EndLength(len) => subchunk_uinteger_bytes(0x32, *len),
                XID666Subchunk::FadeLength(len) => subchunk_uinteger_bytes(0x33, *len),
                XID666Subchunk::MutedVoices(muted) => subchunk_data_bytes(0x33, *muted),
                XID666Subchunk::LoopAmount(looped) => subchunk_data_bytes(0x33, *looped),
                XID666Subchunk::MixingLevel(level) => subchunk_data_bytes(0x33, *level),
            };
            chunk_size += &bytes.len().try_into()?;
            self.write_all(&bytes)?;
        }
        self.seek(SeekFrom::Start(0x10204))?;
        self.write_all(&chunk_size.to_le_bytes())?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::error::Error;
    use crate::xid666::XID666Read;
    use crate::xid666::XID666Subchunk;
    use crate::xid666::XID666Write;
    use crate::xid666::XID666;

    use byteorder::LittleEndian;
    use byteorder::ReadBytesExt;
    use byteorder::WriteBytesExt;
    use chrono::NaiveDate;

    use std::env;
    use std::fs::File;
    use std::io::Cursor;
    use std::io::Read;
    use std::io::Seek;
    use std::io::SeekFrom;
    use std::io::Write;
    use std::path::Path;

    const STRING_IDS: [u8; 7] = [1, 2, 3, 4, 7, 0x10, 0x13];
    const INT_IDS: [u8; 5] = [5, 0x30, 0x31, 0x32, 0x33];
    const DATA_IDS: [u8; 7] = [6, 0x11, 0x12, 0x14, 0x34, 0x35, 0x36];

    struct Subchunk<'a> {
        id: u8,
        type_: u8,
        data: u16,
        vec: &'a [u8],
    }

    trait XID666Manage {
        fn add_subchunk(&mut self, sub: Subchunk);
        fn reset(&mut self);
        fn len(&mut self) -> u32;
    }

    impl<T: Read + Write + Seek> XID666Manage for T {
        fn reset(&mut self) {
            self.seek(SeekFrom::Start(0x10200)).unwrap();
            self.write_all("xid6".as_bytes()).unwrap();
            self.write_u32::<LittleEndian>(0).unwrap();
        }

        fn len(&mut self) -> u32 {
            self.seek(SeekFrom::Start(0x10204)).unwrap();
            self.read_u32::<LittleEndian>().unwrap()
        }

        fn add_subchunk(&mut self, sub: Subchunk) {
            self.seek(SeekFrom::Start(0x10204)).unwrap();
            let len = self.read_u32::<LittleEndian>().unwrap();
            let end = 0x10208 + len;
            self.seek(SeekFrom::Start(end.into())).unwrap();
            self.write_u8(sub.id).unwrap();
            self.write_u8(sub.type_).unwrap();
            self.write_u16::<LittleEndian>(sub.data).unwrap();
            let mut new_written = 4u32;
            if sub.vec.len() != 0 {
                self.write_all(&sub.vec).unwrap();
                let data_len: u32 = sub.vec.len().try_into().unwrap();
                new_written += data_len;
                if sub.vec.len() % 4 != 0 {
                    let padding: u32 = 4 - data_len % 4;
                    new_written += padding;
                    let padding: usize = padding.try_into().unwrap();
                    self.write_all(&vec![0; padding]).unwrap();
                }
            }
            let len = len + new_written;
            self.seek(SeekFrom::Start(0x10204)).unwrap();
            self.write_u32::<LittleEndian>(len).unwrap();
        }
    }

    fn setup(filename: &str) -> Cursor<Vec<u8>> {
        let root = env::var("CARGO_MANIFEST_DIR").unwrap();
        let test_dir = Path::new(&root).join("resources/test");
        let mut file = File::open(test_dir.join(filename)).unwrap();
        let mut data = Vec::new();
        file.read_to_end(&mut data).unwrap();
        Cursor::new(data)
    }

    fn test_parse(data: &mut (impl Read + Seek)) -> XID666 {
        let xid666 = data.read_xid666();
        assert!(xid666.is_ok());
        let xid666 = xid666.unwrap().unwrap();
        assert_eq!(xid666.chunk.len(), 6);
        xid666
    }

    fn test_subchunks(xid666: &XID666) {
        for subchunk in xid666 {
            match subchunk {
                XID666Subchunk::SongName(name) => assert_eq!(name, "Fake Title"),
                XID666Subchunk::GameName(name) => assert_eq!(name, "Fake Game"),
                XID666Subchunk::DateDumped(date) => {
                    assert_eq!(date, &NaiveDate::from_ymd_opt(2020, 6, 7).unwrap())
                }
                XID666Subchunk::CopyrightYear(year) => assert_eq!(year, &2020),
                XID666Subchunk::LoopLength(len) => assert_eq!(len, &255),
                XID666Subchunk::Comments(comment) => assert_eq!(
                    comment,
                    concat!("ABC123213213213",
                    "2132154648adsa65s4d2a6sf54a6d51a5sd1as65d1as65d1as6d16a8w1d6a5d1s6a51d6as81d",
                    "6s51d6a51da6s5d1a6s51d6a5s1d6as51d65as1d65as1d6sa51d65as1d65asdFAKEFAKEFAKEF",
                    "AKEFAKEFAKEKEFAKEFAKEasd")
                ),
                _ => panic!(),
            }
        }
    }

    #[test]
    fn test_binary() {
        let mut spc = setup("binary.spc");
        let xid666 = test_parse(&mut spc);
        test_subchunks(&xid666);
    }

    #[test]
    fn test_binary_write_xid666() {
        let mut spc = setup("binary.spc");
        let xid666 = test_parse(&mut spc);
        test_subchunks(&xid666);

        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        spc.write_xid666(&xid666).unwrap();
        let xid666 = spc.read_xid666().unwrap();
        test_subchunks(&xid666.unwrap());
    }

    #[test]
    fn test_text() {
        let mut spc = setup("text.spc");
        let xid666 = test_parse(&mut spc);
        test_subchunks(&xid666);
    }

    #[test]
    fn test_text_write_xid666() {
        let mut spc = setup("text.spc");
        let xid666 = test_parse(&mut spc);
        test_subchunks(&xid666);

        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        spc.write_xid666(&xid666).unwrap();
        let xid666 = spc.read_xid666().unwrap();
        test_subchunks(&xid666.unwrap());
    }

    #[test]
    fn test_bad1() {
        // This should be a file with the same length as binary.spc, but filled with garbage, so a
        // completely invalid SPC file
        let mut spc = setup("bad1.spc");
        let xid666 = spc.read_xid666();
        assert!(matches!(xid666, Err(Error::Parse)));
    }

    #[test]
    fn test_bad2() {
        // This should be a size 0 file
        let mut spc = setup("bad2.spc");
        let xid666 = spc.read_xid666();
        assert!(matches!(xid666, Ok(None)));
    }

    #[test]
    fn test_good_strings() {
        let mut spc = Cursor::new(Vec::new());

        spc.reset();
        assert!(0u32 == spc.len());
        // Add a min size entry
        let sub = Subchunk {
            id: 1,
            type_: 1,
            data: 4,
            vec: &"HIHI".as_bytes(),
        };
        spc.add_subchunk(sub);
        assert!(8u32 == spc.len());

        // Add an entry that requires some padding
        let sub = Subchunk {
            id: 2,
            type_: 1,
            data: 6,
            vec: &"IHHI44".as_bytes(),
        };
        spc.add_subchunk(sub);
        assert!(20u32 == spc.len());

        // require an entry that does not require padding
        let sub = Subchunk {
            id: 3,
            type_: 1,
            data: 8,
            vec: &"12345678".as_bytes(),
        };
        spc.add_subchunk(sub);
        assert!(32u32 == spc.len());

        let xid666 = spc.read_xid666().unwrap().unwrap();
        for subchunk in &xid666 {
            match subchunk {
                XID666Subchunk::SongName(name) => assert_eq!(name, "HIHI"),
                XID666Subchunk::GameName(name) => assert_eq!(name, "IHHI44"),
                XID666Subchunk::ArtistName(name) => assert_eq!(name, "12345678"),
                _ => panic!(),
            }
        }

        // And now make sure all IDs that can be strings properly parse out as strings
        spc.reset();
        for i in STRING_IDS {
            let sub = Subchunk {
                id: i,
                type_: 1,
                data: 4,
                vec: &"HIHI".as_bytes(),
            };
            spc.add_subchunk(sub);
        }
        let xid666 = spc.read_xid666();
        assert!(xid666.is_ok());
        assert_eq!(spc.len(), 56);
        assert_eq!(xid666.unwrap().unwrap().len(), 7);
    }

    #[test]
    fn test_bad_empty_string() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let sub = Subchunk {
            id: 1,
            type_: 1,
            data: 2,
            vec: &[],
        };
        spc.add_subchunk(sub);

        let xid666 = spc.read_xid666();
        assert!(matches!(xid666, Err(Error::Parse)));

        spc.reset();
        let sub = Subchunk {
            id: 1,
            type_: 1,
            data: 0,
            vec: &[],
        };
        spc.add_subchunk(sub);

        let xid666 = spc.read_xid666();
        assert!(matches!(xid666, Err(Error::Parse)));
    }

    #[test]
    fn test_bad_short_string() {
        let mut spc = Cursor::new(Vec::new());
        spc.reset();
        let sub = Subchunk {
            id: 1,
            type_: 1,
            data: 5,
            vec: &"HIHIHI".as_bytes(),
        };
        spc.add_subchunk(sub);
        let xid666 = spc.read_xid666().unwrap().unwrap();
        for subchunk in &xid666 {
            match subchunk {
                XID666Subchunk::SongName(name) => assert_eq!(name, "HIHIH"),
                _ => panic!(),
            }
        }
        // This second subchunk will be added to the chunk as though it only had 4 bytes of data,
        // but the actual chunk will register as having more data left, so it will fail to parse as
        // the parser will attempt to extract another subchunk, which isn't there
        let sub = Subchunk {
            id: 1,
            type_: 1,
            data: 4,
            vec: &"HIIII".as_bytes(),
        };
        spc.add_subchunk(sub);
        let xid666 = spc.read_xid666();
        assert!(xid666.is_err());
    }

    #[test]
    fn test_bad_string_id_type() {
        let mut spc = Cursor::new(Vec::new());

        // First check that for all valid string IDs, we can't parse if the type is wrong
        for id in STRING_IDS {
            for i in 0..=255 {
                if i == 1 {
                    continue;
                }
                spc.reset();
                let sub = Subchunk {
                    id,
                    type_: i,
                    data: 5,
                    vec: &"ABCDE".as_bytes(),
                };
                spc.add_subchunk(sub);

                let xid666 = spc.read_xid666();
                assert!(matches!(xid666, Err(Error::Parse)));
            }
        }

        // Now check for all non-string IDs to ensure these can never be parsed as a string
        for id in 0..=255 {
            if STRING_IDS.contains(&id) {
                continue;
            }
            spc.reset();
            let sub = Subchunk {
                id,
                type_: 1,
                data: 5,
                vec: &"ABCDE".as_bytes(),
            };
            spc.add_subchunk(sub);

            let xid666 = spc.read_xid666();
            assert!(matches!(xid666, Err(Error::Parse)));
        }
    }

    #[test]
    fn test_bad_integer_id_type() {
        let mut spc = Cursor::new(Vec::new());

        // First check that for all valid integer IDs, we can't parse if the type is wrong
        for id in INT_IDS {
            for i in 0..=255 {
                if i == 4 {
                    continue;
                }
                spc.reset();
                let sub = Subchunk {
                    id,
                    type_: i,
                    data: 4,
                    vec: &0xFFFFFFi32.to_le_bytes(),
                };
                spc.add_subchunk(sub);

                let xid666 = spc.read_xid666();
                assert!(matches!(xid666, Err(Error::Parse) | Err(Error::IO(_))));
            }
        }

        // Now check for all non-integer IDs to ensure these can never be parsed as an integer
        for id in 0..=255 {
            if INT_IDS.contains(&id) {
                continue;
            }
            spc.reset();
            let sub = Subchunk {
                id,
                type_: 4,
                data: 4,
                vec: &0xFFFFFFi32.to_le_bytes(),
            };
            spc.add_subchunk(sub);

            let xid666 = spc.read_xid666();
            assert!(matches!(xid666, Err(Error::Parse)));
        }
    }

    #[test]
    fn test_bad_data_id_type() {
        let mut spc = Cursor::new(Vec::new());

        // First check that for all valid data IDs, we can't parse if the type is wrong
        for id in DATA_IDS {
            for i in 1..=255 {
                spc.reset();
                let sub = Subchunk {
                    id,
                    type_: i,
                    data: 4,
                    vec: &[],
                };
                spc.add_subchunk(sub);

                let xid666 = spc.read_xid666();
                assert!(matches!(xid666, Err(Error::Parse) | Err(Error::IO(_))));
            }
        }

        // Now check for all non-data IDs to ensure these can never be parsed as data
        for id in 0..=255 {
            if DATA_IDS.contains(&id) {
                continue;
            }
            spc.reset();
            let sub = Subchunk {
                id,
                type_: 0,
                data: 4,
                vec: &[],
            };
            spc.add_subchunk(sub);

            let xid666 = spc.read_xid666();
            assert!(matches!(xid666, Err(Error::Parse)));
        }
    }
}
